package com.example.johanbackstrom.tictactoe;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Random;

/**
 * The activity which contains all game logic and draws the board.
 */
public class GameActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private static final String playersMove = "'s move";
    private static final String playerWin = " wins!";
    private static final String gameTie = "It's a tie!";

    private int boardSize = 0;
    private int scoreToWin = 0;
    private int board[][];
    private int numberOfPlayers = 0;
    private int currentPlayer = 0;
    private int startingPlayer = 0;

    private TextView gameInfo;
    private TextView playerOneWins;
    private TextView ties;
    private TextView playerTwoWins;

    private String playerOne = null;
    private String playerTwo = null;

    private boolean won;

    /**
     * Creates the activity and prepares the game board and view based on the information in the SharedPreference
     * marked as "currentGameSettings".
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        sp = getSharedPreferences("currentGameSettings", Context.MODE_PRIVATE);
        numberOfPlayers = sp.getInt("numberOfPlayers", 0);

        boardSize = sp.getInt("size", 0);
        if (boardSize == 3) {
            scoreToWin = 3;
        } else if (boardSize == 5) {
            scoreToWin = 4;
        } else if (boardSize == 7) {
            scoreToWin = 5;
        }
        board = new int[boardSize][boardSize];

        currentPlayer = 1;
        startingPlayer = 1;

        if (numberOfPlayers > 1) {
            playerOne = sp.getString("player1", null);
            playerTwo = sp.getString("player2", null);
        } else {
            playerOne = sp.getString("player1", null);
            playerTwo = "Computer";
        }

        gameInfo = (TextView) findViewById(R.id.game_information);
        gameInfo.setText(playerOne + playersMove);
        gameInfo.setTextColor(Color.BLUE);

        TextView playerOneWinsText = (TextView) findViewById(R.id.player_one_wins_text);
        playerOneWinsText.setText(playerOne + " wins:");
        playerOneWins = (TextView) findViewById(R.id.player_one_wins);

        ties = (TextView) findViewById(R.id.ties);

        TextView playerTwoWinsText = (TextView) findViewById(R.id.player_two_wins_text);
        playerTwoWinsText.setText(playerTwo + " wins:");
        playerTwoWins = (TextView) findViewById(R.id.player_two_wins);

        drawBoard();
    }

    /**
     * Method for creating the ImageViews used as the board.
     */
    private void drawBoard() {
        LinearLayout boardContainer = (LinearLayout)findViewById(R.id.board_container);
        boardContainer.setGravity(Gravity.BOTTOM);

        TableRow.LayoutParams cellParams = new TableRow.LayoutParams(
                0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f
        );

        TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );

        TableLayout table = new TableLayout(this);
        table.setStretchAllColumns(true);

        for(int i = 0; i < boardSize; i++){

            TableRow row = new TableRow(this);
            row.setLayoutParams(rowParams);

            for(int j = 0; j < boardSize; j++) {
                ImageView tile = new ImageView(this);

                //The tag is used for identifying the row and col of the ImageView when pressed.
                String rowCol = Integer.toString(i) + Integer.toString(j);
                tile.setTag(rowCol);
                tile.setId(i * 10 + j);
                tile.setPadding(0, 0, 0, 0);
                tile.setLayoutParams(cellParams);
                tile.setScaleType(ImageView.ScaleType.FIT_CENTER);
                tile.setAdjustViewBounds(true);
                tile.setImageResource(R.drawable.empty_tile);
                tile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tilePressed(v);
                    }
                });

                row.addView(tile);
            }

            table.addView(row);
        }

        boardContainer.addView(table);

    }

    /**
     * Method which is called when one of the boards ImageViews are pressed. Checks that the tile
     * is not already marked and if not, adds the appropriate image the view before checking for a winner.
     */
    private void tilePressed(View v) {
        ImageView tile = (ImageView) v;
        String rowCol = (String) tile.getTag();

        int row = Integer.parseInt(rowCol.substring(0, 1));
        int col = Integer.parseInt(rowCol.substring(1));
        int tileValue = board[row][col];

        if (tileValue == 0) {
            if (currentPlayer == 1) {
                tile.setImageResource(R.drawable.x_tile);
                board[row][col] = 1;
            } else if (currentPlayer == 2) {
                tile.setImageResource(R.drawable.o_tile);
                board[row][col] = 2;
            }
            checkForWinner(row, col);
        }
        return;
    }

    /**
     * Method that checks if there is a winner, the game is a tie or if is the next user's turn.
     * Changes the view according to the result.
     */
    public void checkForWinner(int row, int col) {
        won = checkRow(row);
        if (!won) {
            won = checkColumn(col);
            if (!won) {
                won = checkDiagonals(row, col);
            }
        }
        if (won) {
            if (currentPlayer == 1) {
                gameInfo.setText(playerOne + playerWin);
                gameInfo.setTextColor(Color.BLUE);
                int currentWins = Integer.parseInt(playerOneWins.getText().toString());
                playerOneWins.setText(Integer.toString(currentWins + 1));
                updateHighscore(playerOne);
            } else if (currentPlayer == 2) {
                gameInfo.setText(playerTwo + playerWin);
                gameInfo.setTextColor(Color.RED);
                int currentWins = Integer.parseInt(playerTwoWins.getText().toString());
                playerTwoWins.setText(Integer.toString(currentWins + 1));
                if (numberOfPlayers != 1) {
                    updateHighscore(playerTwo);
                }
            }
            showNewButton();
        } else if (boardIsFull()) {
            gameInfo.setText(gameTie);
            gameInfo.setTextColor(Color.BLACK);
            int currentTies = Integer.parseInt(ties.getText().toString());
            ties.setText(Integer.toString(currentTies + 1));
            if (currentPlayer == 2) {
                currentPlayer = 1;
            } else {
                currentPlayer = 2;
            }
            showNewButton();
        } else if (currentPlayer == 1) {
            gameInfo.setText(playerTwo + playersMove);
            gameInfo.setTextColor(Color.RED);
            currentPlayer = 2;
            if (numberOfPlayers == 1) {
                computerMove();
            }
        } else {
            gameInfo.setText(playerOne + playersMove);
            gameInfo.setTextColor(Color.BLUE);
            currentPlayer = 1;
        }
    }

    /**
     * Selects a tile for the computer at random.
     */
    public void computerMove() {
        do {
            Random rand = new Random();
            int row = rand.nextInt(boardSize);
            int col = rand.nextInt(boardSize);
            int id = row * 10 + col;
            ImageView tile = (ImageView) findViewById(id);
            tilePressed(tile);
        } while (currentPlayer != 1 && !won);
    }

    /**
     * Checks the row to see if currentPlayer has won.
     */
    public boolean checkRow(int row) {
        int counter = 1;
        int currentTile = board[row][0];

        for (int i = 1; i < boardSize; i++) {
            if (currentTile == board[row][i] && currentTile == currentPlayer) {
                counter++;
                if (counter == scoreToWin) {
                    return true;
                }
            } else {
                counter = 1;
            }
            currentTile = board[row][i];
        }
        return false;
    }

    /**
     * Checks the column to see if currentPlayer has won.
     */
    public boolean checkColumn(int col) {
        int counter = 1;
        int currentTile = board[0][col];

        for (int i = 1; i < boardSize; i++) {
            if (currentTile == board[i][col] && currentTile == currentPlayer) {
                counter++;
                if (counter == scoreToWin) {
                    return true;
                }
            } else {
                counter = 1;
            }
            currentTile = board[i][col];
        }
        return false;
    }

    /**
     * Checks the appropriate diagonals to see if currentPlayer has won.
     */
    public boolean checkDiagonals(int row, int col) {
        int counter = 1;
        int currentTile;
        int startCol = col;
        int startRow = row;
        int nextRow;
        int nextCol;

        //First checks the upper right to lower left diagonal.
        while (startRow != 0 && startCol > 0) {
            if (startCol == 0) {
                break;
            }
            startCol--;
            startRow--;
        }
        currentTile = board[startRow][startCol];

        nextCol = startCol + 1;
        nextRow = startRow + 1;
        while (nextCol < boardSize && nextRow < boardSize) {
            if (currentTile == board[nextRow][nextCol] && currentTile == currentPlayer) {
                counter++;
                if (counter == scoreToWin) {
                    return true;
                }
            } else {
                counter = 1;
            }
            currentTile = board[nextRow][nextCol];
            nextRow++;
            nextCol++;
        }

        //Checks the lower left to upper right diagonal.
        counter = 1;
        startRow = row;
        startCol = col;
        while ((startCol != (boardSize - 1)) && (startRow > 0)) {
            startCol++;
            startRow--;
        }
        currentTile = board[startRow][startCol];

        nextCol = startCol - 1;
        nextRow = startRow + 1;
        while (nextCol >= 0 && nextRow < boardSize) {
            if (currentTile == board[nextRow][nextCol] && currentTile == currentPlayer) {
                counter++;
                if (counter == scoreToWin) {
                    return true;
                }
            } else {
                counter = 1;
            }
            currentTile = board[nextRow][nextCol];
            nextRow++;
            nextCol--;
        }
        return false;
    }

    /**
     * Method used to see if the board is full.
     */
    public boolean boardIsFull() {
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (board[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Method used to show the new_game_button and disable the listeners for the ImageViews on the board.
     * Called by checkForWinner when game is over.
     */
    public void showNewButton() {
        Button newGameButton = (Button) findViewById(R.id.new_game_button);
        newGameButton.setVisibility(View.VISIBLE);
        LinearLayout boardContainer = (LinearLayout)findViewById(R.id.board_container);
        disableTileOnClick(boardContainer);
    }

    /**
     * Disables the listeners for the ImageViews on the board.
     */
    public void disableTileOnClick(ViewGroup viewGroup){
        int cnt = viewGroup.getChildCount();
        for (int i = 0; i < cnt; i++){
            View v = viewGroup.getChildAt(i);
            if (v instanceof ViewGroup){
                disableTileOnClick((ViewGroup) v);
            } else {
                v.setOnTouchListener(null);
                v.setOnClickListener(null);
            }
        }
    }

    /**
     * Resets the board and prepares a new game when new_game_button is pressed. Changes starting
     * order of the players.
     */
    public void clearAndRestart(View view) {
        Button newGameButton = (Button) view;
        newGameButton.setVisibility(View.INVISIBLE);
        board = new int[boardSize][boardSize];

        if (startingPlayer == 1) {
            startingPlayer = 2;
            currentPlayer = 2;
            gameInfo.setText(playerTwo + playersMove);
            gameInfo.setTextColor(Color.RED);
        } else {
            startingPlayer = 1;
            currentPlayer = 1;
            gameInfo.setText(playerOne + playersMove);
            gameInfo.setTextColor(Color.BLUE);
        }
        LinearLayout boardContainer = (LinearLayout)findViewById(R.id.board_container);
        boardContainer.removeAllViews();
        drawBoard();
        if (numberOfPlayers == 1 && currentPlayer == 2) {
            computerMove();
        }
    }

    /**
     * Updates the highscore for the player that won.
     */
    public void updateHighscore(String playerName) {
        SharedPreferences players = getSharedPreferences("scoreList", Context.MODE_PRIVATE);
        SharedPreferences.Editor pEditor = players.edit();
        int currentScore = players.getInt(playerName, 0);
        pEditor.putInt(playerName, currentScore + 1);
        pEditor.commit();
    }

}


