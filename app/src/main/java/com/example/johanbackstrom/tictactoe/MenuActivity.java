package com.example.johanbackstrom.tictactoe;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * The main activity which launches at the applications start.
 */
public class MenuActivity extends AppCompatActivity {
    private SharedPreferences.Editor editor;

    /**
     * Creates the activity which contains buttons for selecting number of players or going to the highscore.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //SharedPreferences are used to send data between the activities.
        SharedPreferences sp = getSharedPreferences("currentGameSettings", Context.MODE_PRIVATE);
        editor = sp.edit();

        Button twoPlayersButton = (Button) findViewById(R.id.two_players_button_id);
        twoPlayersButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editor.clear();
                editor.commit();
                editor.putInt("numberOfPlayers", 2);
                editor.commit();
                goToSettings();
            }
        });

        Button onePlayerButton = (Button) findViewById(R.id.one_player_button_id);
        onePlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editor.clear();
                editor.commit();
                editor.putInt("numberOfPlayers", 1);
                editor.commit();
                goToSettings();
            }
        });
    }

    /**
     * Sends the user to the GameSettingsActivity.
     */
    private void goToSettings() {
        Intent intent = new Intent(this, GameSettingsActivity.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the HighscoreActivity.
     */
    public void goToHighscore(View view) {
        Intent intent = new Intent(this, HighscoreActivity.class);
        startActivity(intent);
    }

}
