package com.example.johanbackstrom.tictactoe;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ToggleButton;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The activity where the user inserts player name(s) and selects board size.
 */
public class GameSettingsActivity extends AppCompatActivity {
    private int numberOfPlayers = 0;
    private int boardSize = 3;
    private EditText playerName;
    private SharedPreferences.Editor editor;

    private ToggleButton threeButton;
    private ToggleButton fiveButton;
    private ToggleButton sevenButton;

    /**
     * Creates the activity and adds Buttons and EditText(s) for inserting name(s), selecting board size and starting a game.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamesettings);

        SharedPreferences sp = getSharedPreferences("currentGameSettings", Context.MODE_PRIVATE);
        editor = sp.edit();
        numberOfPlayers = sp.getInt("numberOfPlayers", 0);

        LinearLayout editTextView = (LinearLayout) findViewById(R.id.edit_text_view);
        for (int i = 0; i < numberOfPlayers; i++) {
            playerName = new EditText(this);
            playerName.setHint(R.string.insert_player_text);
            playerName.setId(10000 + i);
            editTextView.addView(playerName);
        }

        threeButton = (ToggleButton) findViewById(R.id.three_board);
        threeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ToggleButton) v).isChecked()) {
                    boardSize = 3;
                    threeButton.setEnabled(false);
                    fiveButton.setEnabled(true);
                    sevenButton.setEnabled(true);

                    fiveButton.setChecked(false);
                    sevenButton.setChecked(false);
                }
            }
        });

        fiveButton = (ToggleButton) findViewById(R.id.five_board);
        fiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ToggleButton) v).isChecked()) {
                    boardSize = 5;
                    fiveButton.setEnabled(false);
                    threeButton.setEnabled(true);
                    sevenButton.setEnabled(true);

                    threeButton.setChecked(false);
                    sevenButton.setChecked(false);
                }
            }
        });

        sevenButton = (ToggleButton) findViewById(R.id.seven_board);
        sevenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ToggleButton) v).isChecked()) {
                    boardSize = 7;
                    sevenButton.setEnabled(false);
                    threeButton.setEnabled(true);
                    fiveButton.setEnabled(true);

                    threeButton.setChecked(false);
                    fiveButton.setChecked(false);
                }
            }
        });

    }

    /**
     * Called when the user presses the play_button. Controls that input for names are okay (unique with no whitespace),
     * and adds the user to the highscore list if it has not been registered before. Starts the GameActivity.
     */
    public void startGame(View view) {
        SharedPreferences playerList = getSharedPreferences("scoreList", Context.MODE_PRIVATE);
        String nameString = null;
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher;
        boolean whiteSpaceFound = false;

        //Go through all player names inputed into EditTexts and add them to the SharedPreference marked currentGameSettings
        //if input is ok.
        for (int i = 0; i < numberOfPlayers; i++) {
            playerName = (EditText) findViewById(10000 + i);
            String temp = nameString;
            nameString = playerName.getText().toString();
            matcher = pattern.matcher(nameString);
            whiteSpaceFound = matcher.find();
            if (whiteSpaceFound || nameString.length() < 1 || nameString.equals(temp)) {
                makeAlert();
                return;
            }
            //Check if player name has been used before, otherwise add it to highscore list.
            int registeredScore = playerList.getInt(nameString, -1);
            if (registeredScore == -1) {
                SharedPreferences.Editor rpEditor = playerList.edit();
                rpEditor.putInt(nameString, 0);
                rpEditor.commit();
            }
            editor.putString("player" + Integer.toString(i + 1), nameString);
        }

        editor.putInt("size", boardSize);
        editor.commit();
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    /**
     * Shows an AlertDialog if the user if an inserted name is not unique or has whitespace.
     * Called from startGame(View view).
     */
    private void makeAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please insert unique player names, do not use whitespace.");
        builder.setCancelable(true);

        builder.setPositiveButton(
                "Got it!",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        return;
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

}
