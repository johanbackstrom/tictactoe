package com.example.johanbackstrom.tictactoe;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * An activity which contains a highscore list of all user's with a registered name.
 */
public class HighscoreActivity extends AppCompatActivity {

    /**
     * Creates the activity and adds all names in the highscore list ordered by highest score.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        //The SharedPreferences file marked "scoreList" contains all registered users and their score.
        SharedPreferences sp = getSharedPreferences("scoreList", Context.MODE_PRIVATE);

        String name;
        int score;
        Map<String, ?> map = sp.getAll();
        TreeMap<String, Integer> playerMap = new TreeMap<String, Integer>();

        //The users names and scores are placed in a TreeMap.
        String scoreString;
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            scoreString = entry.getValue().toString();
            playerMap.put(entry.getKey(), Integer.parseInt(scoreString));
        }

        //The TreeMap is sorted before the highscore list is filled.
        TreeMap<String, Integer> sortedPlayerMap = SortByValue(playerMap);
        Map.Entry<String, Integer> mapEntry;
        LinearLayout listView = (LinearLayout) findViewById(R.id.names_and_scores);
        //Each players name and score is added to a TextView
        for (int i = 0; i < playerMap.size(); i++) {
            TextView listEntryText = new TextView(this);
            mapEntry = sortedPlayerMap.pollFirstEntry();
            name = mapEntry.getKey();
            score = mapEntry.getValue();
            listEntryText.setText(i+1+ ". " + name + ": " + Integer.toString(score));
            listEntryText.setTextColor(Color.BLACK);
            listEntryText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            listView.addView(listEntryText);
        }

    }

    /**
     * Using the class ScoreComparator this method orders the map.
     */
    public static TreeMap<String, Integer> SortByValue (TreeMap<String, Integer> map) {
        ScoreComparator sc =  new ScoreComparator(map);
        TreeMap<String,Integer> sortedMap = new TreeMap<String,Integer>(sc);
        sortedMap.putAll(map);
        return sortedMap;
    }

}

/**
 * Class used for comparing values when sorting the TreeMap.
 */
class ScoreComparator implements Comparator<String> {

    Map<String, Integer> map;

    public ScoreComparator(Map<String, Integer> scoreList) {
        this.map = scoreList;
    }

    public int compare(String a, String b) {
        if (map.get(a) >= map.get(b)) {
            return -1;
        } else {
            return 1;
        }
    }
}